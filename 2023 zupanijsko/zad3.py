# Lift

# reprezentirano koje sve lampice svijetle kada se radi o određenom broju
broj0 = ["A", "B", "C", "D", "E", "F"]
broj1 = ["B", "C"]
broj2 = ["A", "B", "G", "E", "D"]
broj3 = ["A", "B", "G", "C", "D"]
broj4 = ["F", "G", "B", "C"]
broj5 = ["A", "F", "G", "C", "D"]
broj6 = ["A", "F", "G", "E", "D", "C"]
broj7 = ["A", "B", "C"]
broj8 = ["A", "B", "C", "D", "E", "F", "G"]
broj9 = ["A", "B", "C", "D", "F", "G"]
# lista u listi, sadrži sve lise kako prikazujemo određeni broj
osvjetljenja = [broj0, broj1, broj2, broj3, broj4, broj5, broj6, broj7, broj8, broj9]


zgrada = int(input())
# promotrimo sto radimo kada imamo samo 1 zgradu
niz1 = input()
niz1_lista = []
for slovo in niz1:
    niz1_lista.append(slovo)

moguci_brojevi1 = 0
# prođimo po listi osvjetljenja, svaki element te liste je lista
# koja sadrži kombinaciju slova za jedan od brojava
for broj in osvjetljenja:
    # gledamo za pojedinu listu slova od broja nalaze li se sve svijetleće lampice u toj listi,
    # ako da moguće je da taj broj svjetli
    if all(slovo in broj for slovo in niz1_lista):
        moguci_brojevi1 += 1
rezultat = moguci_brojevi1

if zgrada == 2:
    # ponovimo za drugi niz isti postupak
    niz2 = input()
    niz2_lista = []
    for slovo in niz2:
        niz2_lista.append(slovo)

    moguci_brojevi2 = 0
    for broj in osvjetljenja:
        if all(slovo in broj for slovo in niz2_lista):
            moguci_brojevi2 += 1

    # konačni rezultat nam je tu umnožak jer za svaki mogući prvi svjetleći broj
    # je moguće da svijetli bilokoji od drugog broja, kombinatorika
    rezultat = rezultat * moguci_brojevi2

print(rezultat)