# Bodovi

# liste
# ulazne varijable
b1 = int(input())
b2 = int(input())
b3 = int(input())
x = int(input())

# sve kombinacije zbrajanja dodajemo u listu i mogućnost da je riješio sve krivo
moguci_bodovi = []
moguci_bodovi.append(0)
moguci_bodovi.append(b1)
moguci_bodovi.append(b2)
moguci_bodovi.append(b3)
moguci_bodovi.append(b1+b2)
moguci_bodovi.append(b1+b3)
moguci_bodovi.append(b2+b3)
moguci_bodovi.append(b1+b2+b3)

# mičemo neki broj ako se pojavio više puta
bez_duplikata = []
for broj_bodova in moguci_bodovi:
    if broj_bodova not in bez_duplikata:
        bez_duplikata.append(broj_bodova)

# dodajemo sve brojeve u jedan string kako bi ispisali u redu
prvi_red = ""
for broj_bodova in bez_duplikata:
    prvi_red = prvi_red + str(broj_bodova) + " "
print(prvi_red)

# ispisujemo ako se nalazi zadani broj kao jedna kombinacija
if x in bez_duplikata:
    print("DA")
else:
    print("NE")



# setovi
moguci_bodovi_set = set()
moguci_bodovi_set.add(0)
moguci_bodovi_set.add(b1)
moguci_bodovi_set.add(b2)
moguci_bodovi_set.add(b3)
moguci_bodovi_set.add(b1+b2)
moguci_bodovi_set.add(b1+b3)
moguci_bodovi_set.add(b2+b3)
moguci_bodovi_set.add(b1+b2+b3)

print(moguci_bodovi_set)
prvi_red = ""
for broj_bodova in moguci_bodovi_set:
    prvi_red = prvi_red + str(broj_bodova) + " "

print(prvi_red)
if x in moguci_bodovi_set:
    print("DA")
else:
    print("NE")