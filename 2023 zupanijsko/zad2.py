# Prekid

duljina_utakmice = int(input())
zabijeni_golovi = input()
primljeni_golovi = input()

# radimo listu od ulaza (string s razmacima)
zabijeni_golovi_lista = zabijeni_golovi.split(" ")
primljeni_golovi_lista = primljeni_golovi.split(" ")
ukupno_zabijeni_golovi = 0
ukupno_primljeni_golovi = 0
# pošto je ulaz bio string i elementi liste su string, mi želimo raditi s brojevima
# pa zato svaki element liste mjenjamo iz stringa u int
for i in range(0, len(zabijeni_golovi_lista)):
    zabijeni_golovi_lista[i] = int(zabijeni_golovi_lista[i])
for i in range(0, len(primljeni_golovi_lista)):
    primljeni_golovi_lista[i] = int(primljeni_golovi_lista[i])

# zbrajamo svaki puta kada u listi danih golova naiđemo na 1
for gol in zabijeni_golovi_lista:
    if gol == 1:
        ukupno_zabijeni_golovi += 1

for gol in primljeni_golovi_lista:
    if gol == 1:
        ukupno_primljeni_golovi += 1

print(ukupno_zabijeni_golovi, ukupno_primljeni_golovi)

# najbolja razlika je barem u prvoj minuti
najbolja_razlika = zabijeni_golovi_lista[0] - primljeni_golovi_lista[0]
najbolja_minuta = 1
zabijeni_gol = 0
primljeni_gol = 0
for i in range(0, duljina_utakmice):
    # zbrajamo koliko smo ukupno dali golova u i-toj minuti
    zabijeni_gol += zabijeni_golovi_lista[i]
    primljeni_gol += primljeni_golovi_lista[i]
    # gledamo ako je razlika s novim golom bolja za nas
    if zabijeni_gol - primljeni_gol > najbolja_razlika:
        najbolja_minuta = i+1
        najbolja_razlika = zabijeni_gol - primljeni_gol
    # ako je razlika ista
    elif zabijeni_gol - primljeni_gol == najbolja_razlika:
        # ako vodimo ili je jednako želimo gledati što dulje
        if najbolja_razlika >= 0:
            najbolja_minuta = i + 1
        # ako gubimo ne želimo gledati ako smo primili više golova
        else:
            if not primljeni_golovi_lista[i] == 1:
                najbolja_minuta = i + 1

if (najbolja_razlika > 0):
    print("W")
elif (najbolja_razlika < 0):
    print("L")
else:
    print("D")

print(najbolja_minuta)