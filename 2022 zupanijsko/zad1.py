# Wordle

zamisljeni_niz = input()
broj_pokusaja = int(input())
sve_boje = []
# Pitamo za onoliko unosa koliko je pokušaja rekao da će biti
for i in range(broj_pokusaja):
    pokusaj_niz = input()
    pokusaj_boje = ""
    # Prolazimo kroz svaki broj od pokušaja
    for i in range(len(pokusaj_niz)):
        # ako je na istom mjestu na kojem je broj u pokušaju na tom mjestu u broj u zamišljenom onda je pogodio
        if pokusaj_niz[i] == zamisljeni_niz[i]:
            pokusaj_boje += "G"
        # ako nije na dobrom mjestu provjeravamo je li pokušani broj postoji u zamišljenom broju
        elif pokusaj_niz[i] in zamisljeni_niz:
            pokusaj_boje += "Y"
        else:
            pokusaj_boje += "R"
    sve_boje.append(pokusaj_boje)

for boje in sve_boje:
    print(boje)