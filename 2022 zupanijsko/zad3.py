# Vampir
import itertools as it

# previše komplicirano, objasniti usmeno
broj = int(input())
n_str = str(broj)
num_iter = it.permutations(n_str, len(n_str))

sva_rjesenja = []
for num_list in num_iter:
    v = ''.join(num_list)
    x, y = v[:int(len(v)/2)], v[int(len(v)/2):]
    if x[-1] == '0' and y[-1] == '0':
        continue
    if int(x) * int(y) == int(n_str):
        sva_rjesenja.append(x + y)

if sva_rjesenja:
    min_zbroj = 10000
    A = 0
    B = 0
    for rjesenje in sva_rjesenja:
        if (int(rjesenje[0:3]) + int(rjesenje[3:6])) < min_zbroj:
            A = min(int(rjesenje[0:3]), int(rjesenje[3:6]))
            B = max(int(rjesenje[0:3]), int(rjesenje[3:6]))
            min_zbroj = int(rjesenje[0:3]) + int(rjesenje[3:6])

    print(A, B)
else:
    zbroj = 0
    for br in n_str:
        zbroj += int(br)
    print(zbroj)