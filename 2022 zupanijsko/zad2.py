# Veselje
dan_u_tjednu = int(input())
tjedan_u_godini = int(input())
dana_do_veselja = int(input())

# oduzimamo 1 jer ako je 5. tjedan proslo je 4 tjedna i nekoliko dana
tjedan_u_godini = tjedan_u_godini - 1

# u primjeru vidimo da koriste da svaki mjesec ima točno 4 tjedana, odnosno 28 dana, nije po pravom kalendaru
mjesec = int(tjedan_u_godini/4)
# uzimamo koji je dan u tjednu + ostatak tjedana koji nije korišten za računanje mjeseca
dan = dan_u_tjednu + (tjedan_u_godini%4)*7
print(dan, mjesec+1)

# ukupno dana do rodendana od 1.1.
dana_do_rodendana = dan_u_tjednu + tjedan_u_godini*7 + dana_do_veselja
mjesec_rodendana = int(dana_do_rodendana/28)
dan_rodendana = dana_do_rodendana - mjesec_rodendana*28
# pripaziti da rođendan može biti u sljedećoj godini pa gledamo ostatak djeljenja s 12
konacno_mjesec_rodendana = int((mjesec_rodendana+1)%12)
print(dan_rodendana, konacno_mjesec_rodendana)

rodendan_dan_u_tjednu = dan_rodendana % 7
if (rodendan_dan_u_tjednu == 1):
    print("PONEDJELJAK")
    print(dan_rodendana - 0, konacno_mjesec_rodendana, dan_rodendana + 6, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 2):
    print("UTORAK")
    print(dan_rodendana - 1, konacno_mjesec_rodendana, dan_rodendana + 5, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 3):
    print("SRIJEDA")
    print(dan_rodendana - 2, konacno_mjesec_rodendana, dan_rodendana + 4, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 4):
    print("CETVRTAK")
    print(dan_rodendana - 3, konacno_mjesec_rodendana, dan_rodendana + 3, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 5):
    print("PETAK")
    print(dan_rodendana - 4, konacno_mjesec_rodendana, dan_rodendana + 2, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 6):
    print("SUBOTA")
    print(dan_rodendana - 5, konacno_mjesec_rodendana, dan_rodendana + 1, konacno_mjesec_rodendana)
elif (rodendan_dan_u_tjednu == 0):
    print("NEDJELJA")
    print(dan_rodendana - 6, konacno_mjesec_rodendana, dan_rodendana + 0, konacno_mjesec_rodendana)
