N = int(input())

# 1. broj nam je na kojem tvu se pusta projekcija (zapravo nije bitno za zadatak)
# 2. broj je kod za film koji se pusta
# 3. broj je koji se put film pusta na toj televiziji
# zapravo nam je bitno ako je zadnji broj 1
# ako nije 1 ona znaci da je repriza i moramo dodati u niz u kojem spremamo pustene filmove, svaki film samo jednom
# ako je 1 provjerimo ako je film u nizu s pustenim filmovima, ako nije onda je prva premijera, inace je premijera
broj_prvih_premijera = 0
broj_premijera = 0
broj_repriza = 0
pustani_filmovi = ""
for i in range(N):
    projekcija = input()
    if (projekcija[2] == "1" and projekcija[1] not in pustani_filmovi):
        broj_prvih_premijera += 1
        pustani_filmovi += projekcija[1]
    elif (projekcija[2] == "1" and projekcija[1] in pustani_filmovi):
        broj_premijera += 1
    elif (projekcija[2] != "1"):
        broj_repriza += 1
        if (projekcija[1] not in pustani_filmovi):
            pustani_filmovi += projekcija[1]



print(broj_prvih_premijera)
print(broj_premijera)
print(broj_repriza)
